const { Client } = require('pg');
const config = require('../Config/dev');


let instance;

module.exports = (function () {
  function createDbInstance() {

    let client = new Client({
        connectionString: config.Heroku_PostgresConnURL.remote_URL
    });
    
    client.connect();
    return client;
  }

  return {
    getInstance: function () {
      if (!instance) {
        instance = createDbInstance();
      }
      return instance;
    }
  };
})();