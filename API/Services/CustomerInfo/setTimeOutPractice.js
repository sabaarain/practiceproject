const express = require("express");

exports.setTimeOutPractice = async (err, res) => {
    
    const myArray = ['One', 'Two', 'Three'];
  myArray.myMethod = function (Property){
      console.log(arguments.length > 0 ? this[Property] : this);
      
    
  }
myArray.myMethod();
myArray.myMethod(1);
setTimeout(function(){myArray.myMethod()}, 2.0 * 1000);
setTimeout(function(){myArray.myMethod('1')}, 3.5 * 1000);
setTimeout(() => {myArray.myMethod()}, 2.0*1000); // prints "zero,one,two" after 2 seconds
setTimeout(() => {myArray.myMethod('2')}, 2.5*1000); // prints "one" after 2.5 seconds

// SET time Interval 
var interval_ID = setInterval(() => console.log('multiplication'), 4000);
setTimeout(() => {clearInterval(interval_ID); console.log('multiplication Stop'); }, 6000);


let timerId = setInterval(() => console.log('tick'), 8000);
setTimeout(() => { clearInterval(timerId); console.log('stop'); }, 16000);



let count = 0;
  
const intervalId = setInterval(() => {
  console.log('HELLO SABA');
  count++;
  
  if (count === 5) {
    console.log('Clearing the interval id after 5 executions');
    clearInterval(intervalId);
  }
}, 300);

let counts = 0;
const interval_Id = setInterval(
  (x, y) => {
    console.log(`The sum of ${x} and ${y} is ${x + y}`);
    counts++;
  
    if (counts === 3) {
      console.log("Clearing the interval id after 5 executions");
      clearInterval(interval_Id);
    }
  },
  500,
  5,
  10
);


}