
//slice() extracts a part of a string and returns the extracted part in a new string.
//The method takes 2 parameters: the start position, and the end position (end not included).


let txt = "abcdedfkdfdkmfkvfkf";
let length = txt.length;
console.log(length);

let txt1 = "Apple, Banana, Kiwi ";
let part = txt1.slice(7, 13);
let part2 = txt1.slice(-12, -1);
let part3 = txt1.slice(7);
console.log(part);
console.log(part2);
console.log(part3);

//substring() is similar to slice().
//The difference is that substring() cannot accept negative indexes.

let data = "Mobile, Laptop, Charger";
let part4 = data.substring(6, 14);
console.log(part4);
let part5 = data.substr(8, 7); // 2nd parameter is length
console.log(part5);
let part6 = data.substr(-6);
console.log(part6);



let datas = "Mobile, Laptop, Charger";
let rep = datas.replace(/LAPTOP/i, "Computer"); // /i is use for case insentiveness
console.log(rep);

let text6 = "Please visit Microsoft and Microsoft!";
let newText = text6.replace(/Microsoft/g, "W3Schools");
console.log(newText);

let text7 = "Hello World!";
let text8 = text7.toUpperCase();
console.log(text8);

let txt2 = "ARE YOU PRESENT";
let txt3 = txt2.toLowerCase();
console.log(txt3);


let txt4 = "Web";
let txt5 = "Semantic";
let txt6 = txt4.concat(" ",txt5);
console.log(txt6);

let text11 = "      Hello World!      ";
let text12 = text11.trim();
console.log(text12);

let text13 = "555";
let padded = text13.padStart(8,1);
console.log(padded);

let text14 = "58785";
let padde = text14.padEnd(9,0);
console.log(padde);


//substr() is similar to slice().
//The difference is that the second parameter specifies the length of the extracted part.

