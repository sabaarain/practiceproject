const express = require("express");
const router = express.Router();

const weather= require("../Services/CustomerInfo/weather");
router.get('/users/weather/', weather.weather);
const login = require("../Services/CustomerInfo/login");
router.get("/users/login", login.login);
const note = require("../Services/CustomerInfo/note");
router.post("/users/note", note.note);
const insertData = require("../Services/CustomerInfo/insertData");
router.post("/users/insertData", insertData.insertData);
const deleteData = require("../Services/CustomerInfo/deleteData");
router.post("/users/deleteData", deleteData.deleteData);
const mapArray = require("../Services/CustomerInfo/mapArray");
router.post("/users/mapArray", mapArray.mapArray);
const asyncAwait = require("../Services/CustomerInfo/asyncAwait");
router.post("/users/asyncAwait", asyncAwait.asyncAwait);
const fileOperation = require("../Services/CustomerInfo/fileOperation");
router.post("/users/fileOperation", fileOperation.fileOperation);
const set_TimeOut_Practice = require("../Services/CustomerInfo/setTimeOutPractice");
router.post("/users/set_TimeOut_Practice", set_TimeOut_Practice.setTimeOutPractice);


module.exports = router;
