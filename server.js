const express = require("express");
const router = require("./API/Routes/index");
const app = express();
require("dotenv").config();

PORT = process.env.PORT;

app.use(express.urlencoded({extended : true}));

// Parse JSON bodies (as sent by API clients)
app.use(express.json());
process.env.NODE_TLS_REJECT_UNAUTHORIZED=0
// app.use(express.json());
app.use("/api", router);

app.listen(PORT, () =>
 console.log("info", `server is up and running on port :${PORT}`)
);
